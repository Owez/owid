# owid

A simple GUI-orientated HTML generation system that was made in 1 hour. An example of this is shown below:

```rust
let input_labels_doc = ODoc {
    widgets: vec![
        Label {
            contents: String::from("Hello there!"),
        },
        Label {
            contents: String::from("Another label.."),
        },
    ],
    style: None,
};

assert_eq!(
    String::from("<html><p>Hello there!</p><p>Another label..</p></html>"),
    input_labels_doc.generate_html()
)
```

Essentially, it gives a strict widget-based system for constructing HTML documents effectively. To build this library, please run the following commands:

```bash
git clone https://gitlab.com/owez/owid # clone the repository
cd orwid # change directory into the local repository
cargo build --release # build a release version of the library
```

You should now be able to find the `.rlib` library file in `target/build/libowid.rlib` (using cargo is a better choice, just link the gitlab repository [like so](https://doc.rust-lang.org/cargo/reference/specifying-dependencies.html#specifying-dependencies-from-git-repositories)).
