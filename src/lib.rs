//! An experimental html-based templating engine allowing for interactive GUI
//! systems to be made easily

pub mod widgets;
pub mod templating;
