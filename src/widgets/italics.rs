use crate::templating::{OPart, OWidget, OPartContents};

pub struct Italics {
    pub contents: String,
}

impl OWidget for Italics {
    fn as_opart(&self) -> OPart {
        OPart {
            html_name: String::from("i"),
            arguments: None,
            inner: OPartContents::Text(String::clone(&self.contents))
        }
    }
}
