use crate::templating::{OPart, OWidget, OPartContents};

/// A simple widget that displays a given plaintext message
pub struct Label {
    pub contents: String,
}

impl OWidget for Label {
    fn as_opart(&self) -> OPart {
        OPart {
            html_name: String::from("p"),       
            arguments: None,
            inner: OPartContents::Text(String::clone(&self.contents))
        }
    }
}

mod test {
    use super::*;
    use crate::templating::*;

    #[test]
    fn basic_label() {
        let input_label_doc = ODoc {
            widgets: vec![Label {
                contents: String::from("Hello world!"),
            }],
            style: None,
        };

        assert_eq!(
            String::from("<html><p>Hello world!</p></html>"),
            input_label_doc.generate_html()
        )
    }

    #[test]
    fn multiple_labels() {
        let input_labels_doc = ODoc {
            widgets: vec![
                Label {
                    contents: String::from("Hello there!"),
                },
                Label {
                    contents: String::from("Another label.."),
                },
            ],
            style: None,
        };

        assert_eq!(
            String::from("<html><p>Hello there!</p><p>Another label..</p></html>"),
            input_labels_doc.generate_html()
        )
    }
}
