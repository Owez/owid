use crate::templating::{OPart, OPartContents, OWidget};

pub struct Image {
    src: String,
}

impl OWidget for Image {
    fn as_opart(&self) -> OPart {
        OPart {
            html_name: String::from("img"),
            arguments: Some(
                [(String::from("src"), String::clone(&self.src))]
                    .iter()
                    .cloned()
                    .collect(),
            ),
            inner: OPartContents::None,
        }
    }
}

mod tests {
    use super::*;
    use crate::templating::*;

    #[test]
    fn base_image() {
        let input_image = ODoc {
            style: None,
            widgets: vec![Image {
                src: String::from("https://doc.rust-lang.org/rust-logo1.40.0.png"),
            }],
        };

        assert_eq!(
            String::from("<html><img src=\"https://doc.rust-lang.org/rust-logo1.40.0.png\"></img></html>"),
            input_image.generate_html()
        );
    }
}
