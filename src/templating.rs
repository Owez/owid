//! Templating engine for owid

use std::collections::HashMap;
use std::path::PathBuf;

/// Enum for generation faults in [ODoc] renders
pub enum GenerationError {
    /// When owid cannot retrieve the given [ODoc::style] file
    StyleRetrievalError,
}

pub struct ODoc<T: OWidget> {
    /// Inner top-level [OWidget]s
    pub widgets: Vec<T>,

    /// CSS styling document if any
    pub style: Option<PathBuf>,
}

impl<T: OWidget> ODoc<T> {
    pub fn generate_html(&self) -> String {
        // TODO add styling
        let mut generation_buffer = String::new();

        for widget in self.widgets.iter() {
            generation_buffer.push_str(&widget.as_opart().generate_html());
        }

        format!("<html>{}</html>", generation_buffer)
    }
}

/// Possible contents for [OPart]
pub enum OPartContents {
    /// Plaintext inside of HTML
    Text(String),

    /// Multiple inner parts, can be used for more strucutral elements
    InnerParts(Vec<OPart>),

    /// When the contents are empty
    None,
}

/// Main representation of widgets
pub struct OPart {
    pub html_name: String,
    pub arguments: Option<HashMap<String, String>>,
    pub inner: OPartContents,
}

impl OPart {
    pub fn generate_html(self) -> String {
        let mut generation_buffer = String::new();
        let arguments = self.argument_gen();

        match self.inner {
            OPartContents::InnerParts(x) => {
                for inner_part in x {
                    generation_buffer.push_str(&inner_part.generate_html());
                }
            }
            OPartContents::Text(plaintext) => generation_buffer.push_str(&format!(
                "<{0}{1}>{2}</{0}>",
                self.html_name, arguments, plaintext
            )),
            OPartContents::None => {
                generation_buffer.push_str(&format!("<{0}{1}></{0}>", self.html_name, arguments))
            }
        };

        generation_buffer
    }

    /// Generates HTML arugments for [OPart::generate_html]
    fn argument_gen(&self) -> String {
        if self.arguments.is_some() {
            let mut arg_buf: Vec<String> = Vec::new();

            for (arg_name, arg_content) in self.arguments.as_ref().unwrap() {
                arg_buf.push(format!("{}=\"{}\"", arg_name, arg_content));
            }

            format!(" {}", arg_buf.join(" "))
        } else {
            String::new()
        }
    }
}

/// The main widget implementation
pub trait OWidget {
    /// Renders an individual widget to an [OPart]
    fn as_opart(&self) -> OPart;
}
